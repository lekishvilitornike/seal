import "./../assets/css/corporate.css";
import "./../assets/css/navigation.css";
import "./../assets/css/shortcodes.css";
import "./../assets/css/styles.css";
import "bootstrap/dist/css/bootstrap.min.css";

import * as React from "react";
import { Route, Switch } from "react-router-dom";

import About from "./AboutUs/AboutUs";
import Contact from "./Contact/Contact";
import Home from "./Home/Home";

// import "./../assets/scss/App.scss";
const Main = () => (
  <Switch>
    <Route exact path={"/"} component={Home} />
    <Route exact path={"/about"} component={About} />
    {/* <Route exact path={"/products"} component={Product}/> */}
    <Route exact path={"/contact"} component={Contact} />
  </Switch>
);

export default Main;
