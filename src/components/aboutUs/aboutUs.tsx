import * as React from "react";

import MinHeader from "../shared/MinHeader/Minheader";

const About = () => (
  <div>
    <MinHeader />
    <div className="about">
      <div className="main">
        <div className="section">
          <div className="row">
            <div className="col-md-12">
              <div className="about-content">
                <h3>შ.პ.ს. ჯითიპრინტი</h3>
                <p>
                  შ.პ.ს. ჯითიპრინტი არის ACPLOMB ჯგუფს ოფიციალური წარმომადგენელი საქართველოში.
  ამ ჯგუფში შედის “MEGA FORTRIS”(ავსტრალია), “Security Seal Centre” Ltd-s (სანკტ-პეტერბურგი, რუსეთი) და ООО НПО пломбировочных устройств  (სანკტ-პეტერბურგი, რუსეთი). საკუთარი საწარმოო და სამეცნიერო ბაზა,  ფილიალები მთელ მსოფლიოში ACPLOMB ჯგუფს ერთ-ერთ უძლიერეს კომპანიად აქცევს დალუქვის თანამედროვე სისტემების ბაზარზე.
  ამ ფაქტორების გამო ჩვენმა ფირმამ თავის პარტნიორად ACPLOMB ჯგუფი აირჩია და ურთიერთობის ორი წლის მანძილზე დარწმუნდა თავისი არჩევანის  სისწორეში.
  დღეს ჩვენ შეგვიძლია შემოგთავაზოთ მაღალი ხარისხის, საიმედო და  საქართველოს ბაზრისთვის მისაღების ფასის თანამედროვე თითქმის 40 სახეობის საიდენტიფიკაციო, ძალოვანი, სპეციალური დანიშნულების ლუქები.
  ჩვენი სპეციალისტები მზად არიან თქვენთან ერთად განიხილონ არსებული  ამოცანები და შემოგთავაზონ საჭირო ტექნიკური გადაწყვეტა.
  მომავლისთვის იგეგმება საკუთარი საწარმოო ბაზის შექმნა საქართველოში, რაც გაზრდის შეკვეთის მიწოდების სისწრაფეს, შეამცირებს მზა პროდუქციის ფასს.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div className="main">
        <div className="section">
          <div className="section-title">
            <h2>ჩვენი სერვისები</h2>
            <hr className="center" />
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="icon-boxes-shadow">
                <p>
                  <i className="fa fa-tablet" aria-hidden="true" />
                </p>
                <h3>RESPONSIVE</h3>
                <p>
                  There are many variations of passages of Lorem Ipsum
                  available, but the majority have suffered alteration.
                </p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="icon-boxes-shadow">
                <p>
                  <i className="fa fa-cubes" aria-hidden="true" />
                </p>
                <h3>MULTIPORPOSE</h3>
                <p>
                  There are many variations of passages of Lorem Ipsum
                  available, but the majority have suffered alteration.
                </p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="icon-boxes-shadow">
                <p>
                  <i className="fa fa-code" aria-hidden="true" />
                </p>
                <h3>CLEAN CODE</h3>
                <p>
                  There are many variations of passages of Lorem Ipsum
                  available, but the majority have suffered alteration.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="main counter-background">
        <div className="section">
          <div className="row">
            <div className="col-sm-3 col-md-3">
              <div className="inner-counter">
                <div className="counter-icon">
                  <img src="img/master/bars.png" alt="" />
                </div>
                <div className="counter-statistics">
                  <div className="counter">
                    <h1>3999</h1>
                  </div>
                  <h5>PROJECTS LAUNCHED</h5>
                </div>
              </div>
            </div>
            <div className="col-sm-3 col-md-3">
              <div className="inner-counter">
                <div className="counter-icon">
                  <img src="img/master/sports.png" alt="" />
                </div>
                <div className="counter-statistics">
                  <div className="counter">
                    <h1>7900</h1>
                  </div>
                  <h5>COMPLETE GOALS</h5>
                </div>
              </div>
            </div>
            <div className="col-sm-3 col-md-3">
              <div className="inner-counter">
                <div className="counter-icon">
                  <img src="img/master/people.png" alt="" />
                </div>
                <div className="counter-statistics">
                  <div className="counter">
                    <h1>8999</h1>
                  </div>
                  <h5>HAPPY CUSTOMERS</h5>
                </div>
              </div>
            </div>
            <div className="col-sm-3 col-md-3">
              <div className="inner-counter">
                <div className="counter-icon">
                  <img src="img/master/technology.png" alt="" />
                </div>
                <div className="counter-statistics">
                  <div className="counter">
                    <h1>10092</h1>
                  </div>
                  <h5>CARS IN STOCK</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default About;
