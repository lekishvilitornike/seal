import * as React from "react";
import { Link } from "react-router-dom";

const Header = () => (
  <div className="main-header">
    <div className="main-nav ontop-slider">
      <div className="navbar">
        <nav id="navigation1" className="navigation">
          <div className="nav-header">
            <Link className="nav-logo" to="/">
              <img src="./src/assets/img/images/logo.png" alt="logo" />
            </Link>
            <div className="nav-toggle" />
          </div>

          <div className="nav-menus-wrapper">
            <ul className="nav-menu align-to-right">
              <li>
                <Link to="/">მთავარი</Link>
              </li>
              <li>
                <Link to="/about">ჩვენს შესახებ</Link>
              </li>
              <li>
                <Link to="/products">ჩვენი პროდუქცია</Link>
              </li>
              {/* <li><a href="#">SLIDERS</a></li>
                            <li><a href="#">SHOP</a></li> */}
              <li>
                <Link to="/contact">კონტაქტი</Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>
);
export default Header;
