import * as React from "react";

const MinHeader = () => (
    <div className="pages-header">
        <div className="section-heading">
            <div className="section">
                <div className="span-title">
                    <h1>Contact</h1>
                    <div className="page-map"><p>Home &nbsp;/&nbsp; Contact</p></div>
                </div>
            </div>
        </div>
    </div>
);

export default MinHeader;