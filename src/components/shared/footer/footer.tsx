import * as React from "react";

const Footer = () => (
    <div className="footer">
        <div className="main no-margin-bottom">
            <div className="section">
                <div className="row">
                    <div className="col-md-5 left-column">
                        <div className="footer-logo"><img src="./src/assets/img/images/logo.png" alt="" /></div>
                        <div className="about-footer">
                            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium
                                voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi.</p>
                        </div>
                        <div className="social-footer">
                            <h6>Follow Us</h6>
                            <div className="social-items"><a href="#">
                                <div className="icon-fa"><i className="fa fa-facebook" aria-hidden="true"></i></div>
                            </a></div>
                            <div className="social-items"><a href="#">
                                <div className="icon-fa"><i className="fa fa-twitter" aria-hidden="true"></i></div>
                            </a></div>
                            <div className="social-items"><a href="#">
                                <div className="icon-fa"><i className="fa fa-instagram" aria-hidden="true"></i></div>
                            </a></div>
                            <div className="social-items"><a href="#">
                                <div className="icon-fa"><i className="fa fa-dribbble" aria-hidden="true"></i></div>
                            </a></div>
                        </div>
                    </div>

                    <div className="col-md-5 right-column">
                        <div className="subscribe-footer">
                            <h6>NEWSLETTER</h6>
                            <p>Suscribe to our newsletter and get the lastest scoop right to your inbox!</p>
                            <div className="newsletter-box">
                                <form action="#" method="post" name="sign-up">
                                    <input type="email" className="input" id="email" name="email"
                                        placeholder="Your email address" required />
                                    <input type="submit" className="button" id="submit" value="SIGN UP" />
                                </form>
                            </div>
                            <p className="cursive">Your email is safe with us, we don't spam.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default Footer;

