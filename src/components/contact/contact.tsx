import * as React from "react";

import MinHeader from "../shared/MinHeader/Minheader";

const Contact = () => (
  <div>
    <MinHeader />
    <div className="main">
      <div className="section">
        <div className="row">
          <div className="col-md-5">
            <div className="contact-left">
              <h6>ჩვენი ოფისი</h6>
              <br />
              <p><i className="fas fa-phone"></i> : 2 35 60 66</p>
              <p><i className="fas fa-mobile"></i> : (+995) 599 90 95 11</p>
              <p><i className="fas fa-at"></i> : info@seal.ge</p>
              <br />
              <p>საქართველო თბილისი</p>
              <p>წერეთლის გამზ. 72 II სად.  I სართული</p>
              <br />
              <p>Open hours: 10 AM to 6 PM ორშ-პარ</p>
            </div>
          </div>
          <div className="col-md-7">
            <div className="contact-right map-box">
              {/*style="border:none; box-shadow: 0 15px 30px 0 rgba(0,0,0,0.11),
                         0 5px 15px 0 rgba(0,0,0,0.08);"*/}
              <iframe
                src="https://snazzymaps.com/embed/46048"
                width="100%"
                height="400px"
              />
            </div>
          </div>
        </div>
        <div className="form-box" />
      </div>
    </div>
  </div>
);

export default Contact;
