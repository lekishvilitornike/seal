import * as React from "react";

import Main from "./Main";
import Footer from "./shared/footer/Footer";
import Header from "./shared/Header/Header";

const reactLogo = require("./../assets/img/react_logo.svg");

export interface AppProps {}

export default class App extends React.Component<AppProps, undefined> {
  render() {
    return (
      <div className="app">
        {/*<div id="loader-wrapper">*/}
        {/*<div id="loader"></div>*/}
        {/*</div>*/}
        <Header />
        <div className="wrapper">
          <Main />
        </div>
        <Footer />
        <a href="#0" className="cd-top">
          Top
        </a>
      </div>
    );
  }
}
