import * as React from "react";
import Api from "../../services/Api/Api";
import Carousel from "nuka-carousel";


const Home = () => (

    <section>
        <div className="slider">
            <Carousel withoutControls={true}>
                <img src="./src/assets/img/images/corporate-bg.jpg" alt="" />
                <img src="./src/assets/img/images/corporate-bg-2.jpg" alt="" />
                <img src="./src/assets/img/images/corporate-bg-3.jpg" alt="" />
                {/* <div className="slider__item active">
                    <img src="./src/assets/img/images/corporate-bg.jpg" alt="" />
                    <h2 className="slider__title">lorem ipsume</h2>
                </div>
                <div className="slider__item" style={{ backgroundImage: 'url(./src/assets/img/images/corporate-bg.jpg)' }}>
                    <h2 className="slider__title">lorem ipsume</h2>
                </div>
                <div className="slider__item" style={{ backgroundImage: 'url(./src/assets/img/images/corporate-bg.jpg)' }}>
                    <h2 className="slider__title">lorem ipsume</h2>
                </div> */}
            </Carousel>
        </div>
        <div className="main">
            <div className="section">
                <div className="row">
                    <div className="col-md-5">
                        <div className="corporate-about">
                            <h3>რატომ უნდა აგვირჩიოთ ჩვენ?</h3>
                            <p>
                                ჩვენ გთავაზობთ თანამედროვე,  მაღალი ხარისხის, საიმედო და  საქართველოს   ბაზრისთვის        მისაღები  ფასის,  თითქმის  25 სახეობის  საიდენტიფიკაციო, ძალოვანი,  სპეციალური  დანიშნულების  ლუქებს, სეიფ-პაკეტებს; ღია სავაჭრო  სტენდებზე განთავსებული პროდუქციის დაცვის საშუალებებს.
                            </p>
                            <p>შპს  „ჯითიპრინტის“  საქართველოში საქმიანობის  10 წელის გამოცდილება  საშუალებას  გვაძლევს თქვენთან  ერთად  განვიხილოთ  თქვენს წინაშე არსებული  ამოცანები და  შემოგთავაზოთ შესაბამისი ტექნიკური გადაწყვეტა.</p>
                            <div className="front-signature"><img src="./src/assets/img/images/signature.png" alt="" /></div>

                        </div>
                    </div>
                    <div className="col-md-7">
                        <div className="row">
                            <div className="col-md-6 corporate-box">
                                <img src="http://placehold.it/800x450" alt="" />
                            </div>
                            <div className="col-md-6 corporate-box">
                                <img src="http://placehold.it/800x450" alt="" />
                            </div>
                            <div className="col-md-6 corporate-box">
                                <img src="http://placehold.it/800x450" alt="" />
                            </div>
                            <div className="col-md-6 corporate-box">
                                <img src="http://placehold.it/800x450" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div className="main color-background">
            <div className="section">
                <div className="section-title">
                    <h2>ჩვენი დასალუქი ტექნოლოგიების გამოყენება უზრუნველყოფს</h2>
                    <hr className="center" />
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="corporate-services">
                            <div className="service-icon-img2"><img src="./src/assets/img/master/corporate/risksdecrease.png" alt="" /></div>
                            <h6>სხვადასხვა დანაკარგის რისკის შემცირებას</h6>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="corporate-services">
                            <div className="service-icon-img2"><img src="./src/assets/img/master/corporate/reduced-risk-discover-icon.png" alt="" /></div>
                            <h6>პროდუქციის დაცვას ფალსიფიცირებისაგან</h6>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="corporate-services">
                            <div className="service-icon-img2"><img src="./src/assets/img/master/corporate/matterial.png" alt="" /></div>
                            <h6>მატერიალური ფასეულობის დაცვას</h6>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="corporate-services">
                            <div className="service-icon-img2"><img src="./src/assets/img/master/corporate/No-Terrorism.png" alt="" /></div>
                            <h6>ტერორიზმთან ბრძოლას</h6>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="corporate-services">
                            <div className="service-icon-img2"><img src="./src/assets/img/master/corporate/financialPlanning.png" alt="" /></div>
                            <h6>მატერიალური ფასეულობის ეფექტურ აღრიცხვას</h6>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="corporate-services">
                            <div className="service-icon-img2"><img src="./src/assets/img/master/corporate/exam-icon.png" alt="" />
                            </div>
                            <h6>საქონლის ხარისხის დადასტურებას</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="main">
            <div className="section">
                <div className="section-title">
                    <h2>ჩვენი კლიენტები</h2>
                    <hr className="center" />
                </div>
                <div className="row">
                    <Carousel slidesToShow={5} withoutControls={true}>
                        <img src="./src/assets/img/images/aversi.png" alt="" />
                        <img src="./src/assets/img/images/gwp.jpg" alt="" />
                        <img src="./src/assets/img/images/gncc.jpg" alt="" />
                        <img src="./src/assets/img/images/kartlizing.jpg" alt="" />
                        <img src="./src/assets/img/images/rompetrol.jpg" alt="" />
                        <img src="./src/assets/img/images/sakgheoservice.jpg" alt="" />
                    </Carousel>
                </div>
            </div>
        </div>

        <div className="main gradient-box">
            <div className="section">
                <div className="contact-label">
                    <h1>Get in touch with us</h1>
                    <p>Sed ut perspiciatis unde omnis iste natus error</p>
                </div>
                {/* <div className="contact-front">

                </div> */}
            </div>
        </div>


        <div className="map-container-bottom">
            {/*style="border:none;"*/}
            <iframe src="https://snazzymaps.com/embed/46048" width="100%" height="450px" ></iframe>
        </div>

    </section>
);


export default Home;