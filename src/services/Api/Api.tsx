import * as React from "react";
import Builder from "./Builder";
import Resource from "./resource";


export default class Api {
    resource($url) {
        const resource = new Resource($url);
        return resource;
    }
}