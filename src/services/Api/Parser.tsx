import * as React from "react";

export default class Parser {
    url: string = "";
    builder: any = "";
    constructor(builder) {
        this.builder = builder;
    }
    query() {
        this.Limit();
        this.Fields();
        this.Sorts();
        this.Filters();
        return this.url;
    }
    Limit() {
        if (!this.hasLimit()) {
            return;
        }
        this.url = this.prepend() + "limit=" + this.builder.limitValue;
    }

    Sorts() {
        if (!this.hasSorts()) {
            return;
        }
        this.url = this.prepend() + "sorts=" + this.builder.sorts;
    }
    Fields() {
        if (!this.hasFields()) {
            return;
        }
        this.url = this.prepend() + "fields=" + JSON.stringify(this.builder.fields);
    }
    Filters() {
        if (!this.hasFilters()) {
            return;
        }
        this.url = this.prepend() + "filter=" + JSON.stringify(this.builder.filters);
    }

    hasLimit() {
        return this.builder.limitValue !== null;
    }

    prepend() {
        return (this.url === "") ? "?" : "&";
    }

    hasSorts() {
        return this.builder.sorts.length > 0;
    }

    hasFields() {
        return Object.keys(this.builder.fields.fields).length > 0;
    }

    hasFilters() {
        return Object.keys(this.builder.filters.filter).length > 0;
    }
}