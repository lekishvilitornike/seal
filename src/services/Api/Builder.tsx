import * as React from "react";
import Parser from "./Parser";

export default class Builder {
    limitValue: number = null;
    sorts: Array<any> = [];
    fields: any = { fields: {} };
    filters: any = { filter: {} };
    parser: any;
    constructor() {
        this.parser = new Parser(this);
    }
    query() {
        return this.parser.query();
    }
    select(...fields) {
        if (fields.length === 0) {
            throw new Error("you must enter fields");
        }
        if (fields[0].constructor === String || fields[0].constructor === Array) {
            this.fields.fields = fields.join(",");
        }
        return this;
    }
    where(key, value) {
        if (key === undefined || value === undefined)
            throw new Error("The KEY and VALUE are required on where() method.");

        if (Array.isArray(value) || value instanceof Object)
            throw new Error("The VALUE must be primitive on where() method.");

        this.filters.filter[key] = value;
        return this;
    }
    orderBy (...args) {
        this.sorts = args;

        return this;
    }
    limit (value) {
        if (!Number.isInteger(value)) {
            throw new Error("The VALUE must be an integer on limit() method.");
        }

        this.limitValue = value;

        return this;
    }
}